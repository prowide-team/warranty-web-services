package com.warrantymanager.prowidesolutions.warranty_manager_final;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class Dashboard extends AppCompatActivity implements ClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        ItemData itemsData[] = { new ItemData("Mobile Products",R.drawable.mobile_products),
                new ItemData("Electronics",R.drawable.electronics),
                new ItemData("Home & Kitchen",R.drawable.home_kitchen),
                new ItemData("Kids & Baby",R.drawable.kids_baby),
                new ItemData("Automotive",R.drawable.automotive),
                new ItemData("Jewelry",R.drawable.jewwelry),
                new ItemData("Beauty Appliances",R.drawable.beautyappliances),
                new ItemData("Health & Personal Care",R.drawable.health_personal),
                new ItemData("Patio & Garden",R.drawable.patio_garden),
                new ItemData("Other",R.drawable.other)};

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        MyAdapter mAdapter = new MyAdapter(itemsData);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.setClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Dashboard.this,ProductDetails.class);
                startActivity(intent);
            }
        });

    }
    public void itemClicked(View view,int position)
    {
        Intent i = new Intent(Dashboard.this, CategoryActivity.class);
        i.putExtra("intVariableName", position);
        startActivity(i);
    }
    }

