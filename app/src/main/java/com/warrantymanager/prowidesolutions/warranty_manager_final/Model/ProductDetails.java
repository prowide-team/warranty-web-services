package com.warrantymanager.prowidesolutions.warranty_manager_final;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.firebase.client.ValueEventListener;


public class ProductDetails extends AppCompatActivity {

    Spinner spinner;
    String usernameid;

    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        Firebase.setAndroidContext(this);


        final Button saveBtn = (Button) findViewById(R.id.saveproduct);

        final EditText BrandName = (EditText) findViewById(R.id.ProductBrand);
        final EditText ProductName = (EditText) findViewById(R.id.ProductName);
        final EditText SerialNumber = (EditText) findViewById(R.id.SerialNumber);
        final EditText Username = (EditText) findViewById(R.id.Username);
        final TextView textViewPersons = (TextView)findViewById(R.id.textViewPersons);
Button ls= (Button)findViewById(R.id.listDisplay);
        ls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(ProductDetails.this,itemlisting.class);
                startActivity(x);
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.category, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);


        final DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        saveBtn.setOnClickListener(new View.OnClickListener()

                                   {
                                       @Override
                                       public void onClick(View v) {
                                           Firebase ref = new Firebase(Config.FIREBASE_URL);
                                           if (BrandName.getText().length() == 0 && ProductName.getText().length() == 0 && SerialNumber.getText().length() == 0) {
                                               BrandName.setError("you need to enter brand name");
                                               ProductName.setError("you need to enter product name");
                                               SerialNumber.setError("you need to enter your product serial number");
                                           } else if (BrandName.getText().length() == 0) {
                                               BrandName.setError("Enter a Brand name");
                                           } else if (ProductName.getText().length() == 0) {
                                               BrandName.setError("Enter a Brand name");
                                           } else if (SerialNumber.getText().length() == 0) {
                                               BrandName.setError("Enter a Brand name");
                                           } else {
                                               usernameid = Username.getText().toString();

                                               ProductBean pd = new ProductBean(SerialNumber.getText().toString(), BrandName.getText().toString(), ProductName.getText().toString());
                                               rootRef.child(usernameid).child("Details").setValue(pd);
                                               BrandName.setText("");
                                               ProductName.setText("");
                                               SerialNumber.setText("");
                                               ProductBean person = new ProductBean();





                                           }
                                       }

                                   }
        );
    }

}



