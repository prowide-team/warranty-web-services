package com.warrantymanager.prowidesolutions.warranty_manager_final;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;



    public class CategoryActivity extends AppCompatActivity {
        ListView lvDetail;
        Context context =CategoryActivity.this;
        ArrayList<ListData> myList = new ArrayList<ListData>();
        int item;
        ImageView iv;

        String[] other = new String[]{ "Other Products"   };
        int[] other_img = new int[]{R.drawable.otherprod};

        String[] mob_prod = new String[]{  "My Device", "Other Products"   };
        int[] mob_prod_img = new int[]{   R.drawable.mobile, R.drawable.otherprod   };

        String[] electr = new String[]{ "Television", "Other Products"  };
        int[] electr_img = new int[]{  R.drawable.tv, R.drawable.otherprod  };

        String[] home_kitch = new String[]{ "Refrigrator","Lighting","Blender","Other Products"  };
        int[] home_kitch_img = new int[]{ R.drawable.refrigerator,R.drawable.lamp,R.drawable.blender,R.drawable.otherprod  };

        String[] kids = new String[]{ "Stroller", "Booster", "Other Products"  };
        int[] kids_img = new int[]{  R.drawable.stroller, R.drawable.stroller, R.drawable.otherprod  };

        String[] auto = new String[]{  "Car", "Car Battery", "Other Products"   };
        int[] auto_img = new int[]{  R.drawable.car, R.drawable.car_battery,R.drawable.otherprod  };

        String[] jewel = new String[]{  "Watch", "Diamond Ring", "Other Products"  };
        int[] jewel_img = new int[]{  R.drawable.watch, R.drawable.ring, R.drawable.otherprod  };

        String[] beaut = new String[]{  "Hair dryer", "Shaver", "Other Products"  };
        int[] beaut_img = new int[]{  R.drawable.dryer, R.drawable.shaver, R.drawable.otherprod  };

        String[] health = new String[]{  "Digital Thermometer", "Health Bracelet", "Other Products"   };
        int[] health_img = new int[]{ R.drawable.thermometer, R.drawable.bracelet, R.drawable.otherprod  };

        String[] patio = new String[]{ "Lawnmower", "gash grill", "Other Products"   };
        int[] patio_img = new int[]{ R.drawable.lawnmowe, R.drawable.gas_grill, R.drawable.otherprod  };
        ActionBar actionBar;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_category);
            actionBar = getSupportActionBar();
            lvDetail = (ListView) findViewById(R.id.lvCustomList);
            getDataInList();
            lvDetail.setAdapter(new MyBaseAdapter(context, myList));

        }
        private void getDataInList() {
            Intent mIntent = getIntent();
            int item = mIntent.getIntExtra("intVariableName", 0);

            switch (item) {
                case 0:
                    actionBar.setTitle("Mobile Products");
                    for (int i = 0; i < mob_prod.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(mob_prod[i]);
                        ld.setImgResId(mob_prod_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 1:
                    actionBar.setTitle("Electronics");
                    for (int i = 0; i < electr.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(electr[i]);
                        ld.setImgResId(electr_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 2:
                    actionBar.setTitle("Home & Kitchen");
                    for (int i = 0; i < home_kitch.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(home_kitch[i]);
                        ld.setImgResId(home_kitch_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 3:
                    actionBar.setTitle("Kids & Baby");
                    for (int i = 0; i < kids.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(kids[i]);
                        ld.setImgResId(kids_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 4:
                    actionBar.setTitle("Automotive");
                    for (int i = 0; i < auto.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(auto[i]);
                        ld.setImgResId(auto_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 5:
                    actionBar.setTitle("Jewelry");
                    for (int i = 0; i < jewel.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(jewel[i]);
                        ld.setImgResId(jewel_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 6:
                    actionBar.setTitle("Beauty Appliances");
                    for (int i = 0; i < beaut.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(beaut[i]);
                        ld.setImgResId(beaut_img[i]);
                        myList.add(ld);
                    }
                    break;
                case 7:
                    actionBar.setTitle("Health & Personal Care");
                    for (int i = 0; i < health.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(health[i]);
                        ld.setImgResId(health_img[i]);
                        myList.add(ld);
                    }
                    break;

                case 8:
                    actionBar.setTitle("Patio & Garden");
                    for (int i = 0; i < patio.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(patio[i]);
                        ld.setImgResId(patio_img[i]);
                        myList.add(ld);
                    }break;

                case 9:
                    actionBar.setTitle("Other Products");
                    for (int i = 0; i < other.length; i++) {
                        ListData ld = new ListData();
                        ld.setTitle(other[i]);
                        ld.setImgResId(other_img[i]);
                        myList.add(ld);
                    }
                    break;
            }
        }
    }
