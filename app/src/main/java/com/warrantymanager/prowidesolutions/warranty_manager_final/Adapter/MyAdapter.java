package com.warrantymanager.prowidesolutions.warranty_manager_final;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private com.warrantymanager.prowidesolutions.warranty_manager_final.ItemData[] itemsData;
    private com.warrantymanager.prowidesolutions.warranty_manager_final.ClickListener clicklistener=null;

    public MyAdapter(com.warrantymanager.prowidesolutions.warranty_manager_final.ItemData[] itemsData) {
        this.itemsData = itemsData;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public TextView txtViewTitle;

        public ImageView imgViewIcon;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            itemLayoutView.setOnClickListener(this);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
        }

        @Override
        public void onClick(View v) {
            if (clicklistener != null) {
                clicklistener.itemClicked(v, getAdapterPosition());
            }
        }
    }
    public void setClickListener(com.warrantymanager.prowidesolutions.warranty_manager_final.ClickListener clicklistener ) {
        this.clicklistener = clicklistener ;
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {



        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
    }
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_my_adapter, null);



        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public int getItemCount() {
        return itemsData.length;
    }

}


