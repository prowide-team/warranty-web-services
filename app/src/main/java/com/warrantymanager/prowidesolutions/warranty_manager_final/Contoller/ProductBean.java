package com.warrantymanager.prowidesolutions.warranty_manager_final;

/**
 * Created by prakyat hegde on 8/26/2016.
 */
public class ProductBean {
  private String Product_Brand;
   private String Product_Name;
    private String SerialNumber;


    public ProductBean() {
    }

    public ProductBean(String serialNumber, String product_Brand, String product_Name) {
        Product_Brand = product_Brand;
        Product_Name = product_Name;
        SerialNumber = serialNumber;
    }

    public String getProduct_Brand() {
        return Product_Brand;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setProduct_Brand(String product_Brand) {
        Product_Brand = product_Brand;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }
}
