package com.warrantymanager.prowidesolutions.warranty_manager_final;

/**
 * Created by prakyat hegde on 8/26/2016.
 */
public class ListData {

        String title;
        int imgResId;


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getImgResId() {
            return imgResId;
        }

        public void setImgResId(int imgResId) {
            this.imgResId = imgResId;
        }

    }

