package com.warrantymanager.prowidesolutions.warranty_manager_final;

import android.view.View;

/**
 * Created by prakyat hegde on 8/26/2016.
 */
public interface ClickListener {
    public void itemClicked(View view, int position);
}
